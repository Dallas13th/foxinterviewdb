﻿CREATE TABLE [dbo].[User]
(
	[Id] INT NOT NULL,
	[Name] NVARCHAR(100) NOT NULL,
	[ShoesSizeId] INT NOT NULL,
	CONSTRAINT [PK_User_Id] PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_User_ShoesSizeId__ShoesSize_Id] FOREIGN KEY ([ShoesSizeId]) REFERENCES [ShoesSize] ([Id])
)
