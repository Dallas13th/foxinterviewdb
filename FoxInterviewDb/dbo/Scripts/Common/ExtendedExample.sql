﻿--Достать имена тех, у кого самый часто повторяющийся размер ноги
--Expected result:
----------------
--Kolya Sidorov
--Serega Smirnov
--Sanya Smirnov
--Vitalya Smirnov
--Vladimir Harkonnen
--Ivan Vladof
--------------------

DECLARE @groupedSizes TABLE (Size INT NOT NULL, Occurrences INT NOT NULL)

INSERT INTO @groupedSizes
SELECT ss.Size, count(*) AS Occurrences
FROM [User] u
JOIN ShoesSize ss ON u.ShoesSizeId = ss.Id
GROUP BY ss.Size

DECLARE @mostPopularCount INT = (SELECT MAX(Occurrences) FROM @groupedSizes)

SELECT u.[Name], ss.Size FROM ShoesSize ss
INNER JOIN [User] u ON u.ShoesSizeId = ss.Id
INNER JOIN @groupedSizes gs ON gs.Size = ss.Size
WHERE gs.Occurrences = @mostPopularCount;



			
--Alternative solution
WITH GroupedSizes_CTE (Size, Occurrences)
AS
(
	SELECT ss.Size, count(*) AS Occurrences
	FROM [User] u
	JOIN ShoesSize ss ON u.ShoesSizeId = ss.Id
	GROUP BY ss.Size
)
SELECT u.[Name] AS UserName, ss.Size FROM ShoesSize AS ss
INNER JOIN [User] AS u on u.ShoesSizeId = ss.Id
INNER JOIN GroupedSizes_CTE gs ON gs.Size = ss.Size
WHERE gs.Occurrences = (SELECT MAX(Occurrences) FROM GroupedSizes_CTE)
