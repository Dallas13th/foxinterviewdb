﻿--Достать имена тех, у кого самый часто повторяющийся размер ноги
DECLARE @popularSizes TABLE (Size INT NOT NULL);

INSERT INTO @popularSizes
SELECT TOP (1) ss.Size FROM ShoesSize ss
INNER JOIN [User] u ON u.ShoesSizeId = ss.Id
GROUP BY ss.Size
ORDER BY count(ss.Size) DESC;

SELECT u.[Name] FROM [User] u
INNER JOIN ShoesSize ss ON ss.Id = u.ShoesSizeId
INNER JOIN @popularSizes s ON s.Size = ss.Size;

--Достать имена тех, у кого самый часто повторяющийся размер ноги
WITH Sizes_CTE (ShoesSize)
AS
(
	SELECT TOP (1) ss.Size FROM ShoesSize AS ss
	INNER JOIN [User] AS u on u.ShoesSizeId = ss.Id
	GROUP BY ss.Size
	ORDER BY COUNT(*) DESC
)
SELECT u.[Name] AS UserFullName FROM [User] AS u
INNER JOIN ShoesSize AS ss ON ss.Id = u.ShoesSizeId
INNER JOIN Sizes_CTE AS scte ON scte.ShoesSize = ss.Size