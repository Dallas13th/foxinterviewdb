﻿--1=42 2=36 3=39 4=41 5=46

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 1)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (1, N'Vasya Pupkin', 1)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 2)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (2, N'Vasya Ivanov', 1)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 3)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (3, N'Sanya Ivanov', 2)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 4)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (4, N'Sanya Petrov', 3)
END


IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 5)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (5, N'Kolya Sidorov', 4)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 6)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (6, N'Serega Smirnov', 5)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 7)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (7, N'Sanya Smirnov', 5)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 8)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (8, N'Vitalya Smirnov', 5)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 9)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (9, N'Vladimir Harkonnen', 4)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[User] WHERE [Id] = 10)
BEGIN
  INSERT INTO [dbo].[User] ([Id],[Name], [ShoesSizeId])
  VALUES (10, N'Ivan Vladof', 4)
END