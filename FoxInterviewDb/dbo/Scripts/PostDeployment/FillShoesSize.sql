﻿IF NOT EXISTS (SELECT [Id] FROM [dbo].[ShoesSize] WHERE [Id] = 1)
BEGIN
  INSERT INTO [dbo].[ShoesSize] ([Id],[Size])
  VALUES (1, 42)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[ShoesSize] WHERE [Id] = 2)
BEGIN
  INSERT INTO [dbo].[ShoesSize] ([Id],[Size])
  VALUES (2, 36)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[ShoesSize] WHERE [Id] = 3)
BEGIN
  INSERT INTO [dbo].[ShoesSize] ([Id],[Size])
  VALUES (3, 39)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[ShoesSize] WHERE [Id] = 4)
BEGIN
  INSERT INTO [dbo].[ShoesSize] ([Id],[Size])
  VALUES (4, 41)
END

IF NOT EXISTS (SELECT [Id] FROM [dbo].[ShoesSize] WHERE [Id] = 5)
BEGIN
  INSERT INTO [dbo].[ShoesSize] ([Id],[Size])
  VALUES (5, 46)
END